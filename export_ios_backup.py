import argparse
import os
import sys
import logging
import tkinter as Tkinter
import tkinter.filedialog as tkFileDialog
import tkinter.messagebox as messagebox
sys.path.insert(0, os.path.abspath('../..'))
sys.path.insert(0, os.path.abspath('..'))
import pymobilesupport.iphone_backup


if __name__ == '__main__':
    print('ok')

    parser = argparse.ArgumentParser(description='Exports iPhone backup to disk')
    parser.add_argument('--inpath', type=str,
                        help='Path to the iPhone backup')
    parser.add_argument('--outpath', type=str,
                        help='output directory for exported files')
    args = parser.parse_args()

    if not args.inpath or not args.outpath:
        gui_root = Tkinter.Tk()
        gui_root.withdraw()
        path_to_backup= tkFileDialog.askdirectory(parent=gui_root, title='Select backup folder')
        export_path = tkFileDialog.askdirectory(parent=gui_root, title='Select destination')

        if path_to_backup == "" or export_path == "":
            messagebox.showerror("File path error", "You must provide a backup folder and output folder")
            sys.exit(-1)
    else:
        path_to_backup = args.inpath
        export_path = args.outpath

    if not os.path.exists(path_to_backup):
        print('Backup file "{}" does not exist. Quitting...'.format(path_to_backup))
        sys.exit(-1)
    if not os.path.exists(export_path):
        print('Output path "{}" does not exist. Quitting...'.format(export_path))
        sys.exit(-1)

    logging.basicConfig(handlers=[logging.FileHandler('log.txt', 'w', 'utf-8')],
                        level=logging.WARNING)

    res = pymobilesupport.iphone_backup.iOSBackup(path_to_backup)
    res.export_by_domain(export_path)

