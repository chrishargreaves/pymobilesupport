import os
import re
import shutil
import pymobilesupport.thirdparty.mbdbls
import sqlite3
import logging
sort_fld = 'fullpath'
sort_fmt = '%s'


class iOSBackup(object):

    def __init__(self, path_to_backup_folder):
        self.backup_type = '?'
        self.path_to_backup_folder = path_to_backup_folder
        path_to_mbdb_manifest = os.path.join(path_to_backup_folder, 'Manifest.mbdb')
        path_to_db_manifest = os.path.join(path_to_backup_folder, 'Manifest.db')
        if os.path.exists(path_to_mbdb_manifest):
            self.backup_type = 'mbdb'
            self.process_mbdb_version(path_to_mbdb_manifest)
        elif os.path.exists(path_to_db_manifest):
            self.backup_type = 'db'
            self.process_db_version(path_to_db_manifest)
        else:
            logging.debug('No Manifest.mbdb or Manifest.db found')


    def process_db_version(self, path_to_manifest):
        the_sql = """select fileID, Domain, RelativePath from files"""
        con = sqlite3.connect(path_to_manifest)
        cur = con.cursor()
        cur.execute(the_sql)
        results = cur.fetchall()
        self.lookup = {}
        self.fs = {}

        for each in results:
            filename = each[2]
            domain = each[1]
            obj_type = '-'
            file_id = each[0]
            if obj_type == '-':
                 self.fs[domain, filename] = file_id
                 self.lookup[file_id] = (domain, filename)


        path_to_folder = os.path.dirname(path_to_manifest)
        entries = os.listdir(path_to_folder)
        input_files = 0
        for each_entry in entries:
            full_path = os.path.join(path_to_folder, each_entry)

            if os.path.isdir(full_path):
                file_list = os.listdir(full_path)
                for each in file_list:
                    full_file_path = os.path.join(full_path, each)
                    if os.path.isfile(full_file_path):
                        input_files += 1

        print('Files in input: {}'.format(input_files))



    def process_mbdb_version(self, path_to_manifest):
        """Handles the old MBDB format"""
        self.listing = pymobilesupport.thirdparty.mbdbls.process_mbdb_file(path_to_manifest)
        self.lookup = {}
        self.fs = {}
        for each_offset in self.listing:
            full_path = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fullpath']
            filename = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['filename']
            domain = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['domain']
            obj_type = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['type']
            file_id = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fileID']
            if obj_type == '-':
                self.fs[domain, filename] = file_id
                self.lookup[file_id] = (domain, filename)


    def exists(self, domain, filename):
        if (domain, filename) in self.fs:
            return self.fs[(domain, filename)]
        else:
            return False

    def open(self, domain, full_path):
        src = self.get_abs_path(domain, full_path)
        if src:
            f = open(src, 'rb')
            return f
        else:
            raise FileNotFoundError('{} {} not found'.format(domain, full_path))

    def get_abs_path(self, domain, full_path):
        if self.exists(domain, full_path):
            file_id = self.fs[domain, full_path]
            src = os.path.join(self.path_to_backup_folder, file_id)
            return src
        else:
            return None

    def export_file(self, domain, path, root_export_path):
        if self.exists(domain, path):
            file_id = self.fs[domain, path]
            src = os.path.join(self.path_to_backup_folder, file_id)
            target = os.path.join(root_export_path, domain, path)
            if not os.path.exists(os.path.dirname(target)):
                os.makedirs(os.path.dirname(target))
            shutil.copyfile(src, target)

    def get_file_list(self):
        out_list = []
        for each_offset in self.listing:
            res = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])
            if res['type'] == '-':
                out = "{}".format(res['fullpath'])
                out_list.append(out)
        return out_list

    def get_file_info_list(self):
        out_list = []
        for each_offset in self.listing:
            res = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])
            out_list.append(res)
        return out_list


    def export(self, export_path):
        if not os.path.exists(export_path):
            raise FileExistsError('Export path does not exist')
        out_list     = []
        for each_offset in self.listing:
            full_path = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fullpath']
            domain = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['domain']
            obj_type = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['type']
            file_id = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fileID']
            converted = self.get_path_from_domain(domain, full_path)
            # print('{} -> {}'.format(full_path, converted))
            #out_list.append(res)
            self.export_object(export_path, converted, obj_type, file_id)

    def export_by_domain(self, export_path):
        if not os.path.exists(export_path):
            raise FileExistsError('Export path does not exist')
        out_list = []
        # for each_offset in self.listing:
        #     full_path = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fullpath']
        #     domain = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['domain']
        #     obj_type = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['type']
        #     file_id = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['fileID']
            #print(full_path)

        file_count = 0
        failed_file_count = 0

        for each in self.fs:
            full_path = "{}/{}".format(each[0],each[1])
            obj_type = '-'
            file_id = self.fs[each]

            out_path = re.sub('AppDomainGroup-', 'AppDomainGroup/', full_path)
            out_path = re.sub('AppDomain-', 'AppDomain/', out_path)
            out_path = re.sub('AppDomainPlugin-', 'AppDomainPlugin/', out_path)
            out_path = re.sub('SysSharedContainerDomain-', 'SysSharedContainerDomain/', out_path)

            out_path = re.sub('::', '/', out_path)
            out_path = self.remove_invalid_windows_chars(out_path)
            res = self.export_object(export_path, out_path, obj_type, file_id)
            if res:
                file_count += 1
            else:
                failed_file_count += 1

        print('Files copied: {}'.format(file_count))
        print('Files failed: {}'.format(failed_file_count))


    def remove_invalid_windows_chars(self, in_path):
        return re.sub('[<>:"|?*]', '_', in_path)


    def export_object(self, root_export_path, object_path, fs_type, file_id):
        if fs_type != '-':
            return None
        elif fs_type == '-':
            if self.backup_type == 'mbdb':
                src = os.path.join(self.path_to_backup_folder, file_id)
            elif self.backup_type == 'db':
                src = os.path.join(self.path_to_backup_folder, file_id[0:2], file_id)
            else:
                logging.error('Bad backup type ()'.format(self.backup_type))
                return None

            target = os.path.join(root_export_path, object_path)

            if not os.path.exists(os.path.dirname(target)):
                try:
                    os.makedirs(os.path.dirname(target))
                except FileNotFoundError:
                    logging.error('Most likely path too long to export on Windows. Try a better OS.')
                    logging.error('Skipped {}->{}'.format(src, target))
                    return None

            try:
                shutil.copyfile(src,target)
                logging.info('Copied {} to {}'.format(src, target))
            except FileNotFoundError:
                logging.error('File {} not found (target = {})'.format(src, target))
                return None
            except OSError:
                logging.error('Unknown error with: {} -> {})'.format(src, target))
                return None

            return True

        # can make empty directories if you want to see what apps there are
        # elif fs_type == 'd':
        #     print('making directory {}'.format(object_path))
        #     if not os.path.exists(os.path.join(root_export_path, object_path)):
        #         os.makedirs(os.path.join(root_export_path, object_path))

        else:
            print('unknown object {}'.format(object_path))



    def list_domains(self):
        out_list = []
        for each_offset in self.listing:
            res = pymobilesupport.thirdparty.mbdbls.fileinfo_dict(self.listing[each_offset])['domain']
            if res not in out_list:
                out_list.append(res)
        return out_list

    def get_path_from_domain(self, domain, full_path):
        domain_mapping = {
            'CameraRollDomain': 'private/var/mobile/',
            'DatabaseDomain': 'private/var/db/',
            'HomeDomain': 'private/var/mobile/',
            'KeyboardDomain': 'private/var/mobile/',
            'KeychainDomain': 'private/var/Keychains/',
            'ManagedPreferencesDomain': 'private/var/Managed Preferences/',
            'MediaDomain': 'private/var/mobile/',
            'MobileDeviceDomain': 'private/var/MobileDevice/',
            'RootDomain': 'private/var/root/',
            'SystemPreferencesDomain': 'private/var/preferences/',
            'WirelessDomain': 'private/var/wireless/',
        }

        if domain in domain_mapping:
            return_path = re.sub(domain + '::', domain_mapping[domain], full_path)
        elif re.match('AppDomainGroup', full_path):
            return_path = re.sub('AppDomainGroup-' , 'unknown_path/AppDomainGroup/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('AppDomainPlugin', full_path):
            return_path = re.sub('AppDomainPlugin-' , 'unknown_path/AppDomainPlugin/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('AppDomain', full_path):
            return_path = re.sub('AppDomain-' , 'unknown_path/AppDomain/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('SysSharedContainerDomain', full_path):
            return_path = re.sub('SysSharedContainerDomain-' , 'unknown_path/SysSharedContainerDomain/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('SysContainerDomain', full_path):
            return_path = re.sub('SysContainerDomain-' , 'unknown_path/SysContainerDomain/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('HomeKitDomain', full_path):
            return_path = re.sub('HomeKitDomain' , 'unknown_path/HomeKitDomain/', full_path)
            return_path = re.sub('::', '/', return_path)
        elif re.match('HealthDomain', full_path):
            return_path = re.sub('HealthDomain' , 'unknown_path/HealthDomain/', full_path)
            return_path = re.sub('::', '/', return_path)
        else:
            return_path = re.sub(domain + '::', 'unknown_path/{}'.format(domain), full_path)

        return return_path



