import zlib
import io
import re
import tarfile

class AndroidBackup(object):

    def __init__(self, path_to_adb):
        self.fp = open(path_to_adb, 'rb')
        header = self.fp.read(24)
        if header[0:14] != b'ANDROID BACKUP':
            raise TypeError('Supplied file {} not android backup'.format(path_to_adb))

        self.data = self.fp.read() # reads the whole thing into memory - bad idea for big backups
        self.tarstream = zlib.decompress(self.data)
        self.tf = tarfile.open(fileobj=io.BytesIO(self.tarstream))

    def list_all(self):
        return self.tf.getnames()

    def open_file(self, path_to_file):
        """Returns a file object to the file specified"""
        return self.tf.extractfile(path_to_file)

    def export_to_file_sytem(self, out_path):
        results = self.tf.getmembers()
        for each in results:
            print(each)
            each.name = re.sub(r'[:]', '_', each.name)
            # TODO Log here if you make a change
            self.tf.extract(each, out_path)

        # self.tf.extractall(out_path)


def is_backup_file(path):
    """Returns True if is an android backup"""
    fp = open(path, 'rb')
    if fp.read(14) == b'ANDROID BACKUP':
        return True
    else:
        return False




