from distutils.core import setup
setup(name='pymobilesupport',
      version='0.1',
      description='A library for accessing mobile device ',
      author='Chris Hargreaves',
      author_email='chris@hargs.co.uk',
      py_modules=[
                  'pymobilesupport.android',
                  'pymobilesupport.iphone_backup',
                  'pymobilesupport.thirdparty.mbdbls',
                ],
      )
