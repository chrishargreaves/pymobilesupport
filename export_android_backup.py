#! /usr/local/bin/python3
import argparse
import os
import sys
import tkinter as Tkinter
import tkinter.filedialog as tkFileDialog
import tkinter.messagebox as messagebox

import pymobilesupport.android

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Exports android backup to disk')
    parser.add_argument('-i', '--inpath', type=str,
                        help='Path to the android backup')
    parser.add_argument('-o', '--outpath', type=str,
                        help='output directory for exported files')
    args = parser.parse_args()

    if not args.inpath or not args.outpath:
        gui_root = Tkinter.Tk()
        gui_root.withdraw()
        path_to_backup = tkFileDialog.askopenfilename(parent=gui_root, title='Select backup file')
        export_path = tkFileDialog.askdirectory(parent=gui_root, title='Select destination')

        if path_to_backup == "" or export_path == "":
            messagebox.showerror("File path error", "You must provide a backup file and output folder")
            sys.exit(-1)
    else:
        path_to_backup = args.inpath
        export_path = args.outpath

    if not os.path.exists(path_to_backup):
        print('Backup file "{}" does not exist. Quitting...'.format(path_to_backup))
        sys.exit(-1)
    if not os.path.exists(export_path):
        print('Output path "{}" does not exist. Quitting...'.format(export_path))
        sys.exit(-1)

    adb_backup = pymobilesupport.android.AndroidBackup(path_to_backup)
    list_of_files = adb_backup.list_all()

    for each in list_of_files:
        if each[0] == '/' or each[0] == '\\':
            print('WARNING: may be absolute path in archive ({}). Quitting. '.format(each))
            sys.exit(-1)

    adb_backup.export_to_file_sytem(export_path)

    print('Exported {} objects.'.format(len(list_of_files)))


